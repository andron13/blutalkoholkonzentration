public enum Gender {
	M(1),
	W(2),
	D(3);

	private int gend;

	private Gender(int gend) {
		this.gend = gend;
	}

	public int getGend() {
		return gend;
	}
}