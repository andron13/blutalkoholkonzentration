public class Alkohol {
	private String spirituosenName;
	private double alkoholProzent;
	private double milliLiter;

	public String getSpirituosenName() {
		return spirituosenName;
	}

	public void setSpirituosenName(String spirituosenName) {
		this.spirituosenName = spirituosenName;
	}

	public double getAlkoholProzent() {
		return alkoholProzent;
	}

	public void setAlkoholProzent(double alkoholProzent) {
		this.alkoholProzent = alkoholProzent;
	}

	public double getMilliLiter() {
		return milliLiter;
	}

	public void setMilliLiter(double milliLiter) {
		this.milliLiter = milliLiter;
	}

	public Alkohol(){}

	public Alkohol(String spirituosen, double alkoholProzent, double milliLiter) {
		this.spirituosenName = spirituosen;
		this.alkoholProzent = alkoholProzent;
		this.milliLiter = milliLiter;
	}

	public Alkohol(String spirituosen, double alkoholProzent) {
		this.spirituosenName = spirituosen;
		this.alkoholProzent = alkoholProzent;
		this.milliLiter = milliLiter;
	}

	public Alkohol(double alkoholProzent, double milliLiter) {
		this.alkoholProzent = alkoholProzent;
		this.milliLiter = milliLiter;
	}

	public double findAlkoholInGramm(){
		return milliLiter * alkoholProzent * 0.008;
	}
}
