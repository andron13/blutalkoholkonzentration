import java.util.ArrayList;

public class Roster {
	public static ArrayList createGetraenkeListe() {
		ArrayList<Alkohol> getraenkeListe = new ArrayList<Alkohol>();

		Alkohol bier = new Alkohol("Bier", 4.5);
		Alkohol wein = new Alkohol("Wein", 10.5);
		Alkohol sekt = new Alkohol("Sekt", 9.0);
		Alkohol vodka = new Alkohol("Vodka", 40.0);
		Alkohol whisky = new Alkohol("Whisky", 40.0);
		Alkohol absinth = new Alkohol("Absinth", 42.0);
		Alkohol samogon = new Alkohol("Samogon", 50.0);

		Alkohol kefir = new Alkohol("Kefir", 0.05);
		Alkohol kvas = new Alkohol("Kvas", 0.1);

		Alkohol another = new Alkohol("Schnaps", 20.0);


		getraenkeListe.add(vodka);
		getraenkeListe.add(bier);
		getraenkeListe.add(wein);
		getraenkeListe.add(whisky);
		getraenkeListe.add(sekt);
		getraenkeListe.add(absinth);
		getraenkeListe.add(samogon);
		getraenkeListe.add(another);
		getraenkeListe.add(kvas);
		getraenkeListe.add(kefir);

		return getraenkeListe;
	}

	public static ArrayList createPersonenListe() {

		ArrayList<Person> personenListe = new ArrayList<Person>();

		Person igor = new Person("Игорь", 175, 1985, 5, 25, 68.5, Gender.M );
		Person andrej = new Person("Андрей", 176, 1975, 10, 13, 100, Gender.M );
		Person olga = new Person("Ольга", 170, 1975, 6, 1, 60, Gender.W );
		Person anna = new Person("Анна", 165, 1994, 8, 6, 60, Gender.W );

		personenListe.add(igor);
		personenListe.add(andrej);
		personenListe.add(olga);
		personenListe.add(anna);

		return personenListe;
	}
}
