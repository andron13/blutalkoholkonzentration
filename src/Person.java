import java.time.LocalDate;
import java.time.Period;

/*
 * полезные ссылки
 * http://www.oracle.com/technetwork/articles/java/jf14-date-time-2125367.html
 * https://stackoverflow.com/questions/1116123/how-do-i-calculate-someones-age-in-java
 * http://qaru.site/questions/89449/simpledateformat-and-locale-based-format-string
 *
 */

public class Person{
	private String vorname;
	private int height;
	private int birthDayJahr;
	private int birthDayMonat;
	private int birthDayDay;
	private double gewicht;
	private Gender gender;
	// vers alpha 002, made am 16 August 2018
	private final long VERSIONPERS = 2018_07_05_00_00_02L;

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}


	public int getBirthDayJahr() {
		return birthDayJahr;
	}

	public void setBirthDayJahr(int birthDayJahr) {
		this.birthDayJahr = birthDayJahr;
	}

	public int getBirthDayMonat() {
		return birthDayMonat;
	}

	public void setBirthDayMonat(int birthDayMonat) {
		this.birthDayMonat = birthDayMonat;
	}

	public int getBirthDayDay() {
		return birthDayDay;
	}

	public void setBirthDayDay(int birthDayDay) {
		this.birthDayDay = birthDayDay;
	}

	public double getGewicht() {
		return gewicht;
	}

	public void setGewicht(double gewicht) {
		this.gewicht = gewicht;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public long getVERSIONPERS() {
		return VERSIONPERS;
	}

	public Person() {
	}

	public Person(String vorname, int height, int birthDayJahr, int birthDayMonat, int birthDayDay, double gewicht, Gender gender) {
		this.vorname = vorname;
		this.height = height;
		this.birthDayJahr = birthDayJahr;
		this.birthDayMonat = birthDayMonat;
		this.birthDayDay = birthDayDay;
		this.gewicht = gewicht;
		this.gender = gender;
	}

	public LocalDate findDateOfBirth() {
		LocalDate dateOfBirth = LocalDate.of(birthDayJahr, birthDayMonat, birthDayDay);
		return dateOfBirth;
	}

	public int calculateAge() {

		LocalDate birthDate = findDateOfBirth();
		LocalDate currentDate = LocalDate.now();

		return Period.between(birthDate, currentDate).getYears();
	}
}