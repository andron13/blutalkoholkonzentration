# Алкотестер от andron13

## Полезные ссылки
 
1. http://www.oracle.com/technetwork/articles/java/jf14-date-time-2125367.html
2. https://stackoverflow.com/questions/1116123/how-do-i-calculate-someones-age-in-java
3. http://qaru.site/questions/89449/simpledateformat-and-locale-based-format-string
4. https://de.wikipedia.org/wiki/Blutalkoholkonzentration

##Пояснение к программе
1. Enum Gender сделан для собственного обучения и для коректного сохранения пола человека. И в 
Германии уже вносят вроде тртий пол. 
2. Персона сделана объектом, а вычесления по возможности статическими.
3. Вопрос с алкоголем до конца не решён, на данный момент рассматривается enum, объект, arraylist, 
база данных, и xml файлик. 

##Формулы 

#### Berechnung nach Seidl
Nach Seidl ergeben sich die Reduktionsfaktoren für Frauen (RW) und Männer (RM) unter 
Berücksichtigung von Körpergewicht in kg und von Körperlänge in cm:

RW = 0,31233 − 0,006446 · Körpergewicht + 0,004466 · Körperlänge,
RM = 0,31608 − 0,004821 · Körpergewicht + 0,004432 · Körperlänge.

### Berechnung nach Ulrich
Ulrich schlug für Männer (RU) folgende Beziehung vor:

RU = 0,715 − 0,00462 · Körpergewicht + 0,0022 · Körperlänge

### законы
Nach § 24a Abs. 1 StVG entsprechen 0,5 ‰ Blutalkoholkonzentration 0,25 mg/l Atemalkoholkonzentration.